<?php

 namespace AppBundle\Controller;

 use Symfony\Bundle\FrameworkBundle\Controller\Controller;




 class CircularHandlerFactory extends Controller
 {

  /**
   * @return \Closure
   */
   public static function getId()
   {
     return function ($object) {
       return $object->getId();
     };
   }

 }

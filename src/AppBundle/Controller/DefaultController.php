<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pet;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use GuzzleHttp\Client;

class DefaultController extends Controller
{
    /**
     * @Route("/api", name="api")
     * @Method({"GET"})
     */
    public function indexAction()
    {
      // dump("house");exit;
        //https://api.sunat.cloud/ruc/20602120032
         $endpoint = file_get_contents('http://127.0.0.1:8000/pets/');
         dump($endpoint);exit;
         $client   = new Client();
         $parameters   = ['headers' => ['Content-Type' => 'application/json']];
         $response    = $client->get($endpoint);
         dump($response);exit;
         $res = $response->getBody()->getContents();
         dump($res);exit;
    }

    public function showAction($productId) {
      $product = $this->getDoctrine()
      ->getRepository(Product::class)
      ->find($productId);
      if (!$product) {
        throw $this->createNotFoundException(
          'No product found for id '.$productId
        );
      }
    }
}

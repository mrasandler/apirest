<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pet;
use AppBundle\Entity\Breed;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\Post;


 /**
 * @RouteResource("Pets")
 */

class PetsController extends FOSRestController
{
    private $serializer;
    public function __construct(SerializerInterface $serializer) {
      $this->serializer = $serializer;
    }

    public function cgetAction() {
     
      $repository = $this->getDoctrine()->getRepository(Pet::class);
      // dump($repository);exit;
      $pets = $repository->findAll();
      // dump($pets);exit;
      if ($pets === null) {
        return new View("there are no users", Response::HTTP_NOT_FOUND);
      }
      $view = $this->view($pets, 200);
      return $this->handleView($view);
    }

    public function getAction(int $id) {
      // $view = $this->view($id, 200);
      // return $this->handleView($view);
      $pet = $this->getDoctrine()->getRepository(Pet::class)->find($id);

      if ($pet === null) {
        return new View(null, Response::HTTP_NOT_FOUND);
      }
      return $pet;
    }

    public function postAction(Request $request) {

      $em = $this->getDoctrine()->getManager();

      $obj = new Pet();
      $name = $request->get('name');
      $age = $request->get('age');
      $breed_id = $request->get('breed');
      $breed_obj = $this->getDoctrine()->getRepository(Breed::class)->find($breed_id);

      //dump($breed_czm->getId());exit;
      $user = $request->get('users');

      // dump($request->request->all());exit;
      $obj->setName($name);
      $obj->setAge($age);
      $obj->setBreed($breed_obj);
      // $obj->setUsers($user);
      $em->persist($obj);
      $em->flush();

      // dump($obj->getId());exit;
      $view = $this->view($obj->getId(), 201);
      return $this->handleView($view);
      //return new View("User Added Successfully", Response::HTTP_OK);
    }

    public function deleteAction(int $id) {
      $entityManager = $this->getDoctrine()->getManager();
      $pet = $entityManager->getRepository(Pet::class)->find($id);
      if (!$pet) {
        throw $this->createNotFoundException('No guest found');
      }
      $em = $this->getDoctrine()->getManager();
      $em->remove($pet);
      $em->flush();
      $view = $this->view($id, 200);
      return $this->handleView($view);
    }



    public function redirectAction() {
      $view = $this->redirectView($this->generateUrl('some_route'), 301);
      //or
      $view = $this->routeRedirectView('some_route', array(), 301);
      return $this->handleView($view);
    }

}

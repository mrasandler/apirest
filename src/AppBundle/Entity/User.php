<?php
namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $lastname;

    /**
     * One user has many pets. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Pet", mappedBy="user")
     */
    private $pets;

    public function __construct() {
      $this->pets = new ArrayCollection();
    }

  

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Add pet.
     *
     * @param \AppBundle\Entity\Pet $pet
     *
     * @return User
     */
    public function addPet(\AppBundle\Entity\Pet $pet)
    {
        $this->pets[] = $pet;

        return $this;
    }

    /**
     * Remove pet.
     *
     * @param \AppBundle\Entity\Pet $pet
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePet(\AppBundle\Entity\Pet $pet)
    {
        return $this->pets->removeElement($pet);
    }

    /**
     * Get pets.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPets()
    {
        return $this->pets;
    }
}

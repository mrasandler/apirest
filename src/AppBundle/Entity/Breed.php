<?php
namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="breed")
 */
class Breed
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
    */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $description;

    /**
     * One Breed has many pets. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Pet", mappedBy="breed")
     */
    private $pets;

    public function __construct() {
      $this->pets = new ArrayCollection();
    }

  

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Breed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Breed
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add pet.
     *
     * @param \AppBundle\Entity\Pet $pet
     *
     * @return Breed
     */
    public function addPet(\AppBundle\Entity\Pet $pet)
    {
        $this->pets[] = $pet;

        return $this;
    }

    /**
     * Remove pet.
     *
     * @param \AppBundle\Entity\Pet $pet
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePet(\AppBundle\Entity\Pet $pet)
    {
        return $this->pets->removeElement($pet);
    }

    /**
     * Get pets.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPets()
    {
        return $this->pets;
    }
}
